from django.shortcuts import render

# Method Views

def index(request):
    context = {
        'nav': [
            ['/', 'Home'],
            ['/AboutMe', 'About Me'],
        ],
    }
    return render(request, 'index.html', context)